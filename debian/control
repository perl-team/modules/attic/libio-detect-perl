Source: libio-detect-perl
Section: perl
Priority: optional
Build-Depends: cdbs,
 devscripts,
 perl,
 debhelper,
 dh-buildinfo,
 libsub-exporter-perl,
 liburi-perl,
 libscalar-list-utils-perl (>= 0.24)
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Jonas Smedegaard <dr@jones.dk>
Standards-Version: 3.9.3
Vcs-Git: git://anonscm.debian.org/git/pkg-perl/packages/libio-detect-perl
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libio-detect-perl.git
Homepage: https://metacpan.org/release/IO-Detect

Package: libio-detect-perl
Architecture: all
Depends: ${perl:Depends}, ${misc:Depends}, ${cdbs:Depends}
Suggests: ${cdbs:Suggests}
Description: resolve file name from file handle
 It is stupidly complicated to detect whether a given scalar is a
 filehandle (or something filehandle like) in Perl. IO::Handle attempts
 to do so, but probably falls short in some cases. The primary advantage
 of using this module is that it gives you somebody to blame (me) if
 your code can't detect a filehandle.
 .
 The main use case for IO::Detect is for when you are writing functions
 and you want to allow the caller to pass a file as an argument without
 being fussy as to whether they pass a file name or a file handle.
